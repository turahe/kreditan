<?php

namespace App\Http\Controllers\Providers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

/**
 * Class DuhaController
 * @package App\Http\Controllers\Providers
 */
class DuhaController extends Controller
{
    /**
     * @var string[]
     */
    private $header = [
            "Content-Type" => "application/json",
            "merchantCode" => "KREDITIMPIAN",
            "merchantKey" => "S0k6RklOQU5DRToxNjAxNDU0NDk4"
        ];
    /**
     * @param Request $request
     */
    public function confirmed(Request $request)
    {
        $transaction = DB::connection('mysql')->table('transactions_metadata')
            ->where('id_transaction', $request->get('id_transaction'))->first();

        dd($transaction);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkStatus(Request $request)
    {
        $data = Http::withHeaders($this->header)->post('http://duhasyariah.stagingdevbox.com/api/transaction/check-status', [
            'transactionNumber' => $request->transactionNumber
        ])->json();

        return response()->json(['data' => $data]);
    }

    /**
     * @param array $data
     * @return array|mixed
     */
    private function createTransaction(array $data)
    {
        $orderItems = [
            [
                "name" => $data['name'],
                "type" => "ITEM",
                "quantity" => 1.00,
                "value" => $data['price']
            ],
            [
                "name" => "Biaya Pengiriman",
                "type" => "SHIPPING",
                "quantity" => $data['quantity'],
                "value" => $data['shipping_price']
            ]
        ];


        $postData = array(
            "type" => 'SHOPPING',
            "orderNumber" => $data['orderNumber'],
            "orderDate" => date('Y-m-d H:i:s'),
            "expiredDate" => date('Y-m-d H:i:s', strtotime('tomorrow')),
            "orderGrandTotal" => $data['shipping_price'] + $data['price'],
            "orderItems" => $orderItems
        );

        $url = "http://duhasyariah.stagingdevbox.com/api/transaction/create";

        return Http::withHeaders($this->header)->post($url, $postData)->json();


    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function webhook()
    {
        $post = file_get_contents('php://input');

        $data = json_decode($post);
        $idTransaction = DB::connection('mysql')->table('transactions_metadata')->where('name', 'application')
            ->where('content->transactionNumber', $data->transactionNumber)->first();

        if ($idTransaction) {
            DB::connection('mysql')->table('transactions_logs')->insert([
                'id_transaction' => $idTransaction->id_transaction,
                'status' => 'CONFIRMED_CREDITOR',
                'timestamp' => Carbon::now(),
                'expires' => Carbon::tomorrow(),
                'record_create_timestamp' => Carbon::now(),
                'record_update_timestamp' => Carbon::now()
            ]);

            DB::connection('mysql')->table('sys_modules_users_notifications')->insert([
                'sys_module_user_sender_id' => 0,
                'sys_module_user_recipient_id' => 0,
                'reference_id' => $idTransaction->id_transaction,
                'reference_model' => 'App\Api\Modules\Transactions\Models\Transactions',
                'message' => 'Duha Syariah menyetujui permitaan kredit',
                'metadata' => 'CONFIRMED_CREDITOR'
            ]);

            return response()->json(['data' => 'Data was success to insert']);
        } else {
            return response()->json('id transaction not find', 400);
        }
    }
}
