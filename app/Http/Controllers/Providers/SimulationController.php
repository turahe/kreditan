<?php

namespace App\Http\Controllers\Providers;

use App\Http\Controllers\Controller;
use App\Http\Resources\SimulationResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SimulationController extends Controller
{
    public function __invoke(Request $request)
    {
        switch ($request->idCreditor) {
            default:
                // menggunakan rumus
                /**
                 * 1. ambil data tenor dari db
                 * 2. calculate
                 */
                $priceSale = $request->price;
                $calculateDp = $request->down_payment;
                $interest = $request->interest;
                $value = $request->value;
                $tenor = $request->tenor;
                $range = range(3, $tenor, 1);
                $result = [];
                foreach ($range as $value) {
                    $result[] = [
                        'down_payment' => 0,
                        'installment' => calculate_monthly_flat_rate_installments($priceSale - $calculateDp, $interest, $value)[1],
                        'period' => $value
                    ];
                }

                return response()->json(['data' => $result]);

                break;
            case 1: // Bank Central Asia
                $username = "impiankredit";
                $password = "kredit2020";
                $url = "http://112.78.137.46:7104/SimulasiKredit";
                $postData = array(
                    'UserName' => 'impiankredit',
                    'PassWord' => 'kredit2020',
                    'harga_kendaraan' => $request->harga_kendaraan,
                    'tipe_uang_muka' => '1',
                    'uang_muka' => '30',
                    'kondisi' => '6',
                    'tahun' => '2020',
                    'asuransi' => '2005',
                    'zona' => '3011'
                );


                $dataarray = array(
                    'SimulasiKreditRequest' => $postData
                );
                $data = Http::withHeaders([
                    'Content-Type' => 'application/json;charset=utf-8'
                ])->withBasicAuth($username, $password)->post($url, $dataarray)->json();

                $result = $data['SimulasiKreditResponse']['ADDMList']['ADDM'];

                foreach ($result as $data) {
                    $dataoutput[] = array(
                        "period" => $data['tenor'],
                        "installment" => $data['ph'],
                        "down_payment" => $data['tdp'],
                    );
                }

                return response()->json(['data' => $dataoutput]);

                break;
            case 2: // Duha
                $data = Http::withHeaders(['Content-Type' => 'application/json'])
                    ->post('http://duhasyariah.stagingdevbox.com/api/transaction/installment-simulation', [
                        "type" => "SHOPPING",
                        "price" => $request->price
                    ])
                    ->json();


                $dataoutput = array_map(function ($key) {
                    return [
                        "period" => $key['period'],
                        "installment" => $key['installmentAmount'],
                        "down_payment" => $key['downPaymentAmount'],
                    ];
                }, $data['data']);
                return response()->json(['data' => $dataoutput]);
        }
    }

}
