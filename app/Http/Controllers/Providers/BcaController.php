<?php

namespace App\Http\Controllers\Providers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BcaController extends Controller
{
    public function simulation(Request $request)
    {
        $username = "impiankredit";
        $password = "kredit2020";
        $url = "http://112.78.137.46:7104/SimulasiKredit";
        $postData = array(
            'UserName' => 'impiankredit',
            'PassWord' => 'kredit2020',
            'harga_kendaraan' => $request->harga_kendaraan,
            'tipe_uang_muka' => '1',
            'uang_muka' => '30',
            'kondisi' => '6',
            'tahun' => '2020',
            'asuransi' => '2005',
            'zona' => '3011'
        );


        $dataarray = array(
            'SimulasiKreditRequest'=>$postData
        );
        $data = Http::withHeaders([
            'Content-Type' => 'application/json;charset=utf-8'
        ])->withBasicAuth($username, $password)->post($url, $dataarray)->json();

        $result = $data['SimulasiKreditResponse']['ADDMList']['ADDM'];

        foreach ($result as $data) {
            $dataoutput[]= array(
                "period" => $data['tenor'],
                "installment" => $data['ph'],
                "down_payment" => $data['tdp'],
            );
        }

        return response()->json(['data' => $dataoutput]);
    }
}
